#HSLIDE
##TEAM UPLIFT
#HSLIDE
###Gas Leakage Detector using Arduino, GSM Module, SMS Alert and Sound Alarm.
#HSLIDE
###OBJECTIVES OF THE PROJECT
1.  Detect Gas Leakage (like LPG) using MQ5 Sensor and Arduino
2.  Setup an SMS Alert Mechanism using GSM Module
3.  Send 3 SMS(3 alert messages) to 3 specified mobile numbers(House occupant, Fire service, Insurance company)
4.  Sound Alarm -  produce sound alert on gas leak

#HSLIDE
##COMPONENTS
1.  MQ9 – to sense LPG
2.	Arduino.

...To send MQ9 output and detect gas leak (through level comparison)
...To activate outputs upon gas leak – sound alarm and sms alert
...To send AT commands to GSM Module
...To Turn ON and OFF sound alarm
#HSLIDE
3.	GSM Module – for GSM communication and to send SMS to mobile numbers
4.	Siren – to produce alarm sound.

#HSLIDE
![IMAGE OF PROJECT](http://nokspace.com/uno.jpg "PROJECT")


#HSLIDE
##WHAT MAKES IT STAND OUT?
Most Gas leakage detectors just produce sound alert and that’s all (if occupants are not close by, the siren will keep on producing sounds till the house burns down).

#HSLIDE
## POSSIBLE ADDED FEATURES
Water Sprinkler which is being triggered by Arduino after it detect flame.

#HSLIDE
#THANK YOU

